**TOC**
1. [Installing tools](#1-installing-tools)
2. [Build resources](#2-build-resources)

## 1. Installing tools

### Install Nodejs and npm
```shell
curl -sL https://deb.nodesource.com/setup_5.x | sudo -E shell -
sudo apt-get install nodejs
```

### Install Node modules
Inside `/` directory run the following:
```shell
npm install
```

## 2. Build resources

### User bundle

User build
```shell
npm run user:build
```

User watch
```shell
npm run user:watch
```
