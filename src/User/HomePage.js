import React, { Component, PropTypes } from 'react';
import $ from 'jquery';
import axios from 'axios';
import Map from '../Components/Map/Map.js';
import SearchPanel from '../Components/SearchPanel/SearchPanel.js';
import InfoBanner from '../Components/InfoBanner/InfoBanner.js';
import ServicesList from '../Components/Services/ServicesList.js';
import PropertiesList from '../Components/Properties/PropertiesList.js';
import AgentsList from '../Components/Agents/AgentsList.js';
import Footer from '../Components/Footer/Footer.js';
import { markerGreen } from '../Components/images/images';

import './app.styl';

export default class HomePage extends Component {

  static get NAME() {
    return 'USER_HOME';
  }

  static get contextTypes() {
    return {
      data: React.PropTypes.object,
    };
  }

  static requestData(params, domain = '') {
    return [
      axios.get(`${domain}/api/listings`),
      axios.get(`${domain}/api/brokers`),
    ];
  }

  constructor(props, context) {
    super(props, context);
    const state = {
      brokers: [],
      listings: [],
    };
    this.state = context.data[HomePage.NAME] || state;
  }

  componentDidMount() {
    axios.all(this.constructor.requestData()).then(
      axios.spread((listings, brokers) => {
        const state = Object.assign({}, listings.data, brokers.data);
        this.setState(state);
      })
    );

    setTimeout(() => $('body').removeClass('notransition'), 150);

    if (!(('ontouchstart' in window) ||
        window.DocumentTouch && document instanceof DocumentTouch)) {
      $('body').addClass('no-touch');
    }
  }

  render() {
    const options = { icon: markerGreen, infoBox: true };
    const { listings, brokers } = this.state;
    const markers = listings.map(listing => Object.assign({}, listing, { options }));

    return (
      <div id="hero-container-map">
        <Map id="home-map" markers={markers} />
        <SearchPanel />
        <InfoBanner />
        <div className="home-wrapper">
          <PropertiesList properties={listings} />
          <AgentsList agents={brokers} />
          <ServicesList />
        </div>
        <Footer />
      </div>
    );
  }
}
