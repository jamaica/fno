import { Component, PropTypes } from 'react';
import axios from 'axios';
import { Carousel } from 'react-bootstrap';
import Map from '../Components/Map/Map.js';
import Summary from '../Components/Properties/Single/Summary';
import './single.styl';

export default class Single extends Component {

  static get NAME() {
    return 'USER_VIEW';
  }

  static get contextTypes() {
    return {
      data: React.PropTypes.object,
    };
  }

  static requestData(params, domain = '') {
    const { id } = params;
    return axios.get(`${domain}/api/view/${id}`);
  }

  constructor(props, context) {
    super(props, context);
    console.log("Coontext", context);
    const state = context.data[Single.NAME] || {
      listing: {
        images: [],
        location: [],
      },
    };

    this.state = {...state, index: 0, direction: null};
    this.handleSelect = this.handleSelect.bind(this);
  }

  componentDidMount() {
    const { params } = this.props;
    this.constructor.requestData(params).then((response) => {
      const { listing } = response.data;
      this.setState({listing });
    });
    this.setHeight();
  }

  setHeight() {
    const winHeight = $(window).height();
    $('.user-view-wrapper').height(winHeight);
    $('body').removeClass('no-hidden');
  }

  handleSelect(selectedIndex, e) {
    this.setState({
      index: selectedIndex,
      direction: e.direction
    });
  }

  renderImages(images) {
    const out = images.map((image, key) => {
      return (
        <Carousel.Item key={key}>
          <img width={900} height={500} alt="900x500" src={image} />
        </Carousel.Item>
      );
    });

    return out;
  }

  render() {
    const { listing, index, direction } = this.state;
    const images = this.renderImages(listing.images);
    return (
      <div className="user-view-wrapper">
        <div className="user-view-map mob-min">
          <Map markers={[listing]} />
        </div>
        <div className="user-view-content mob-max">
          <div className="singleTop">
            <Carousel
              activeIndex={index}
              direction={direction}
              onSelect={this.handleSelect}
            >
              {images}
            </Carousel>
            <Summary broker={listing.broker}/>
          </div>
          <div className="clearfix"></div>
          <div className="description">
            <h3>Description</h3>
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum
              tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas
              semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
          </div>
          <div className="share">
            <h3>Share on Social Networks</h3>
            <div className="row">
              <div className="col-xs-6 col-sm-6 col-md-3 col-lg-3 shareItem">
                <a href="#" className="btn btn-sm btn-round btn-o btn-facebook">
                  <span className="fa fa-facebook"/> Facebook
                </a>
              </div>
              <div className="col-xs-6 col-sm-6 col-md-3 col-lg-3 shareItem">
                <a href="#" className="btn btn-sm btn-round btn-o btn-twitter">
                  <span className="fa fa-twitter"/> Twitter
                </a>
              </div>
              <div className="col-xs-6 col-sm-6 col-md-3 col-lg-3 shareItem">
                <a href="#" className="btn btn-sm btn-round btn-o btn-google">
                  <span className="fa fa-google-plus"/> Google+
                </a>
              </div>
              <div className="col-xs-6 col-sm-6 col-md-3 col-lg-3 shareItem">
                <a href="#" className="btn btn-sm btn-round btn-o btn-pinterest">
                  <span className="fa fa-pinterest"/> Pinterest
                </a>
              </div>
            </div>
          </div>
          <div className="amenities">
            <h3>Amenities</h3>
            <div className="row">
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem"><span className="fa fa-car"/> Garage
              </div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem"><span className="fa fa-tint"/> Outdoor Pool
              </div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem inactive"><span className="fa fa-leaf"/> Garden
              </div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem inactive"><span className="fa fa-shield"/>
                Security System
              </div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem"><span className="fa fa-wifi"/> Internet</div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem inactive"><span className="fa fa-phone"/>
                Telephone
              </div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem"><span className="fa fa-asterisk"/> Air
                Conditioning
              </div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem inactive"><span className="fa fa-sun-o"/>
                Heating
              </div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem"><span className="fa fa-fire"/> Fireplace</div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem"><span className="fa fa-arrows-v"/> Balcony
              </div>
              <div className="col-xs-6 col-sm-6 col-md-4 col-lg-4 amItem"><span className="fa fa-desktop"/> TV Cable
              </div>
            </div>
          </div>
          <div className="similar">
            <h3>Similar Properties</h3>
            /*carousel for medium & large devices*/
            <div id="carouselSimilar-1" className="carousel slide visible-lg carousel-col">
              <ol className="carousel-indicators">
                <li data-target="#carouselSimilar-1" data-slide-to="0" className="active"></li>
                <li data-target="#carouselSimilar-1" data-slide-to="1"></li>
              </ol>
              <div className="carousel-inner">
                <div className="item active">
                  <div className="row">
                    <div className="col-xs-4">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/1-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,550,000</div>
                            <span className="icon-eye"> 200</span>
                            <span className="icon-heart"> 54</span>
                            <span className="icon-bubble"> 13</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">FOR SALE</div>
                        </div>
                        <h2>Modern Residence in New York</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 39 Remsen St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 3</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 3430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/2-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,750,000</div>
                            <span className="icon-eye"> 175</span>
                            <span className="icon-heart"> 67</span>
                            <span className="icon-bubble"> 9</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Hauntingly Beautiful Estate</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 169 Warren St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 4430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/3-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,340,000</div>
                            <span className="icon-eye"> 180</span>
                            <span className="icon-heart"> 87</span>
                            <span className="icon-bubble"> 12</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Sophisticated Residence</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 38-62 Water St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 3</li>
                          <li><span className="icon-frame"></span> 2640 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="row">
                    <div className="col-xs-4">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/4-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,930,000</div>
                            <span className="icon-eye"> 145</span>
                            <span className="icon-heart"> 99</span>
                            <span className="icon-bubble"> 25</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Sale</div>
                        </div>
                        <h2>House With a Lovely Glass-Roofed Pergola</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> Wunsch Bldg, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 3</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 2800 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/5-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$2,350,000</div>
                            <span className="icon-eye"> 184</span>
                            <span className="icon-heart"> 120</span>
                            <span className="icon-bubble"> 18</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Luxury Mansion</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 95 Butler St, Brooklyn, NY
                          11231, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 2750 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                    <div className="col-xs-4">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/1-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,550,000</div>
                            <span className="icon-eye"> 200</span>
                            <span className="icon-heart"> 54</span>
                            <span className="icon-bubble"> 13</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">FOR SALE</div>
                        </div>
                        <h2>Modern Residence in New York</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 39 Remsen St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 3</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 3430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <a className="left carousel-control" href="#carouselSimilar-1" role="button" data-slide="prev"><span
                className="fa fa-chevron-left"></span></a>
              <a className="right carousel-control" href="#carouselSimilar-1" role="button" data-slide="next"><span
                className="fa fa-chevron-right"></span></a>
            </div>

            /*carousel for small devices*/
            <div id="carouselSimilar-2" className="carousel slide visible-md carousel-col">
              <ol className="carousel-indicators">
                <li data-target="#carouselSimilar-2" data-slide-to="0" className="active"/>
                <li data-target="#carouselSimilar-2" data-slide-to="1"/>
                <li data-target="#carouselSimilar-2" data-slide-to="2"/>
              </ol>
              <div className="carousel-inner">
                <div className="item active">
                  <div className="row">
                    <div className="col-xs-6">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/1-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,550,000</div>
                            <span className="icon-eye"> 200</span>
                            <span className="icon-heart"> 54</span>
                            <span className="icon-bubble"> 13</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">FOR SALE</div>
                        </div>
                        <h2>Modern Residence in New York</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 39 Remsen St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"/> 3</li>
                          <li><span className="icon-drop"/> 2</li>
                          <li><span className="icon-frame"/> 3430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                    <div className="col-xs-6">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/2-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,750,000</div>
                            <span className="icon-eye"> 175</span>
                            <span className="icon-heart"> 67</span>
                            <span className="icon-bubble"> 9</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Hauntingly Beautiful Estate</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 169 Warren St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 4430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="row">
                    <div className="col-xs-6">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/3-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,340,000</div>
                            <span className="icon-eye"> 180</span>
                            <span className="icon-heart"> 87</span>
                            <span className="icon-bubble"> 12</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Sophisticated Residence</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 38-62 Water St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 3</li>
                          <li><span className="icon-frame"></span> 2640 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                    <div className="col-xs-6">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/4-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,930,000</div>
                            <span className="icon-eye"> 145</span>
                            <span className="icon-heart"> 99</span>
                            <span className="icon-bubble"> 25</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Sale</div>
                        </div>
                        <h2>House With a Lovely Glass-Roofed Pergola</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> Wunsch Bldg, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 3</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 2800 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="row">
                    <div className="col-xs-6">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/5-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$2,350,000</div>
                            <span className="icon-eye"> 184</span>
                            <span className="icon-heart"> 120</span>
                            <span className="icon-bubble"> 18</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Luxury Mansion</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 95 Butler St, Brooklyn, NY
                          11231, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 2750 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                    <div className="col-xs-6">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/1-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,550,000</div>
                            <span className="icon-eye"> 200</span>
                            <span className="icon-heart"> 54</span>
                            <span className="icon-bubble"> 13</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">FOR SALE</div>
                        </div>
                        <h2>Modern Residence in New York</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 39 Remsen St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 3</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 3430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <a className="left carousel-control" href="#carouselSimilar-2" role="button" data-slide="prev"><span
                className="fa fa-chevron-left"></span></a>
              <a className="right carousel-control" href="#carouselSimilar-2" role="button" data-slide="next"><span
                className="fa fa-chevron-right"></span></a>
            </div>

            /*carousel for extra-small devices*/
            <div id="carouselSimilar-3" className="carousel slide visible-xs visible-sm carousel-col">
              <ol className="carousel-indicators">
                <li data-target="#carouselSimilar-3" data-slide-to="0" className="active"></li>
                <li data-target="#carouselSimilar-3" data-slide-to="1"></li>
                <li data-target="#carouselSimilar-3" data-slide-to="2"></li>
                <li data-target="#carouselSimilar-3" data-slide-to="3"></li>
                <li data-target="#carouselSimilar-3" data-slide-to="4"></li>
                <li data-target="#carouselSimilar-3" data-slide-to="5"></li>
              </ol>
              <div className="carousel-inner">
                <div className="item active">
                  <div className="row">
                    <div className="col-xs-12">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/1-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,550,000</div>
                            <span className="icon-eye"> 200</span>
                            <span className="icon-heart"> 54</span>
                            <span className="icon-bubble"> 13</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">FOR SALE</div>
                        </div>
                        <h2>Modern Residence in New York</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 39 Remsen St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 3</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 3430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="row">
                    <div className="col-xs-12">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/2-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,750,000</div>
                            <span className="icon-eye"> 175</span>
                            <span className="icon-heart"> 67</span>
                            <span className="icon-bubble"> 9</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Hauntingly Beautiful Estate</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 169 Warren St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 4430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="row">
                    <div className="col-xs-12">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/3-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,340,000</div>
                            <span className="icon-eye"> 180</span>
                            <span className="icon-heart"> 87</span>
                            <span className="icon-bubble"> 12</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Sophisticated Residence</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 38-62 Water St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 3</li>
                          <li><span className="icon-frame"></span> 2640 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="row">
                    <div className="col-xs-12">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/4-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,930,000</div>
                            <span className="icon-eye"> 145</span>
                            <span className="icon-heart"> 99</span>
                            <span className="icon-bubble"> 25</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Sale</div>
                        </div>
                        <h2>House With a Lovely Glass-Roofed Pergola</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> Wunsch Bldg, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 3</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 2800 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="row">
                    <div className="col-xs-12">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/5-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$2,350,000</div>
                            <span className="icon-eye"> 184</span>
                            <span className="icon-heart"> 120</span>
                            <span className="icon-bubble"> 18</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">For Rent</div>
                        </div>
                        <h2>Luxury Mansion</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 95 Butler St, Brooklyn, NY
                          11231, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 2</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 2750 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="item">
                  <div className="row">
                    <div className="col-xs-12">
                      <div data-linkto="single.html" className="card">
                        <div className="figure">
                          <img src="images/prop/1-1.png" alt="image"/>
                          <div className="figCaption">
                            <div>$1,550,000</div>
                            <span className="icon-eye"> 200</span>
                            <span className="icon-heart"> 54</span>
                            <span className="icon-bubble"> 13</span>
                          </div>
                          <div className="figView"><span className="icon-eye"></span></div>
                          <div className="figType">FOR SALE</div>
                        </div>
                        <h2>Modern Residence in New York</h2>
                        <div className="cardAddress"><span className="icon-pointer"></span> 39 Remsen St, Brooklyn, NY
                          11201, USA
                        </div>
                        <ul className="cardFeat">
                          <li><span className="fa fa-moon-o"></span> 3</li>
                          <li><span className="icon-drop"></span> 2</li>
                          <li><span className="icon-frame"></span> 3430 Sq Ft</li>
                        </ul>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <a className="left carousel-control" href="#carouselSimilar-3" role="button" data-slide="prev"><span
                className="fa fa-chevron-left"></span></a>
              <a className="right carousel-control" href="#carouselSimilar-3" role="button" data-slide="next"><span
                className="fa fa-chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
