import { Router, browserHistory } from 'react-router';
import { render } from 'react-dom';
import routes from './Routes';
import ContextWrapper from '../Components/common/ContextWrapper';

render((
  <ContextWrapper data={window.APP_STATE}>
    <Router history={browserHistory}>
      {routes}
    </Router>
  </ContextWrapper>
), document.getElementById('app-container'));
