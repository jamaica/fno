import { IndexRoute, Route } from 'react-router';
import HomePage from './HomePage.js';
import Single from './Single';
import App from './App.js';
import NoMatch from '../Components/common/NoMatch';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="/view/:id" component={Single} />
    <Route path="*" component={NoMatch} />
  </Route>
);
