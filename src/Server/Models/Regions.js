const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Region = new Schema({
  label: {
    type: String, required: true,
  },
}, {
  collection: 'Regions',
});

export default mongoose.model('Region', Region);
