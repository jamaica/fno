const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');
const User = new Schema({
  username: String,
  password: String,
}, {
  collection: 'users',
});

User.plugin(passportLocalMongoose);

export default mongoose.model('User', User);
