const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Broker = new Schema({
  image: {
    type: String, required: true,
  },
  fullName: {
    type: String, required: true,
  },
  description: {
    type: String, required: true,
  },
  contacts: {
    type: Object, required: true,
  },
}, {
  collection: 'Brokers2',
});

export default mongoose.model('Broker', Broker);
