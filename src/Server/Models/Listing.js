const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Listing = new Schema(
  {
    broker: {
      type: Schema.Types.ObjectId, ref: 'Broker', required: true,
    },
    active: {
      type: Boolean, required: true, default: false,
    },
    hot: {
      type: Boolean, required: true, default: false,
    },
    title: {
      type: String, required: true,
    },
    type: {
      type: String, enum: ['direct', 'sublet'], required: true, default: 'direct',
    },
    sale: {
      type: String, enum: ['sale', 'rent'], required: true, default: 'rent',
    },
    size: {
      type: Number, required: true,
    },
    price: {
      type: Number, required: true,
    },
    location: {
      lat: {
        type: Number, required: true, default: 40.741385,
      },
      lng: {
        type: Number, required: true, default: -74.007103,
      },
      address: {
        type: String, required: true,
      },
      region: {
        type: Schema.Types.ObjectId, ref: 'Region', required: true,
      },
    },
    description: {
      type: String, required: true,
    },
    images: {
      type: [String], required: false,
    },
    created: {
      type: Date, default: Date.now,
    },
  }, {
    collection: 'Listings',
  });

export default mongoose.model('Listing', Listing);
