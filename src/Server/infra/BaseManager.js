import nconf from 'nconf';

const baseManager = {
  handle(app) {
    this.configureCommon(app);
    if (nconf.get('NODE_ENV') === 'production') {
      this.configureProductionEnv(app);
    } else {
      this.configureDevelopmentEnv(app);
    }
  },

  configureCommon() {},

  configureProductionEnv() {},

  configureDevelopmentEnv() {},
};

export default baseManager;
