import fs from 'fs';
import path from 'path';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import morgan from 'morgan';
import nconf from 'nconf';
import fileStreamRotator from 'file-stream-rotator';
import baseManager from './BaseManager';
import paths from '../../../paths';


const middlewareManager = Object.assign({}, baseManager, {
  configureCommon(app) {
    app.use(compression({ threshold: nconf.get('compressionThreshold') }));
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
    app.use(cookieParser('2yj9mx2xs9zX6r8q4A59010cc20m1a9WH'));
    app.use(expressSession({
      secret: '2yj9mx2xs9zX6r8q4A59010cc20m1a9WH',
      resave: false,
      saveUninitialized: false,
    }));
  },

  configureDevelopmentEnv(app) {
    app.use((req, res, next) => {
      res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
      res.header('Expires', '-1');
      res.header('Pragma', 'no-cache');
      next();
    });
  },

  configureProductionEnv(app) {
    const logDir = './' + paths.log;
    if (!fs.existsSync(logDir)) {
      fs.mkdirSync(logDir);
    }
    const accessLogStream = fileStreamRotator.getStream({
      date_format: 'YYYYMMDD',
      filename: path.join(logDir, 'access-%DATE%.log'),
      frequency: 'daily',
      verbose: false,
    });
    app.use(morgan('combined', {
      stream: accessLogStream,
      skip: (req, res) => res.statusCode < 400,
    }));
  },

});

export default middlewareManager;