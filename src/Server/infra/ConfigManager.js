import nconf from 'nconf';
import path from 'path';
import paths from '../../../paths';
import baseManager from './BaseManager';

const configManager = Object.assign({}, baseManager, {

  configureCommon(app) {
    const root = path.resolve('./' + paths.src.server);
    const defaultConfig = path.join(root, 'config/system.json');
    nconf.argv().env().file({ file: defaultConfig });

    app.set('x-powered-by', false);
    app.set('views', path.resolve(root, nconf.get('templateRoot')));
    app.set('view engine', 'pug');
  },
});

export default configManager;
