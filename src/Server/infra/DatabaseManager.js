import nconf from 'nconf';
import baseManager from './BaseManager';

const mongoose = require('mongoose');

const databaseManager = Object.assign({}, baseManager, {
  configureCommon() {
    this.connect();
    mongoose.Promise = global.Promise;
  },

  configureDevelopmentEnv() {
    mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
    mongoose.set('debug', true);
  },

  connect() {
    const url = `mongodb://${nconf.get('databaseDomain')}:${nconf.get('databasePort')}/${nconf.get('databaseTable')}`;
    const options = {
      server: {
        reconnectTries: Number.MAX_VALUE,
        keepAlive: 1,
      },
      user: nconf.get('databaseLogin'),
      pass: nconf.get('databasePassword'),
    };

    return mongoose.connect(url, options);
  },

});

export default databaseManager;
