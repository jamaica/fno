import { match } from 'react-router';

import routes from '../../../User/Routes';
import render from './common/Render';
import mapComponentsToPromises from './common/MapComponentsToPromises';
import prepareData from './common/PrepareData';

const home = (router) => {
  router.get(['/', '/view/:id'], (req, res) => {
    match({ routes, location: req.originalUrl }, (err, redirectLocation, renderProps) => {
      const { promises, components } = mapComponentsToPromises(
        renderProps.components, renderProps.params);
      Promise.all(promises).then((values) => {
        const data = prepareData(values, components);
        const html = render(renderProps, data);
        res.render('index', {
          content: html,
          context: data,
        });
      }).catch((reason) => {
        console.log(reason);
        res.status(500).send(reason);
      });
    });
  });
};

export default [home];
