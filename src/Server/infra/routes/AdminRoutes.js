import passport from 'passport';
import { match } from 'react-router';
import routes from '../../../Admin/Routes';
import mapComponentsToPromises from './common/MapComponentsToPromises';
import prepareData from './common/PrepareData';

const isAuthenticated = (req, res, next) => {
  if (!req.isAuthenticated()) {
    res.redirect('/admin/login');
  }
  return next();
};

const home = (router) => {
  router.get(['/', '/listing', '/listing/:id'], isAuthenticated, (req, res) => {
    match({ routes, location: req.originalUrl }, (err, redirectLocation, renderProps) => {
      const { promises, components } = mapComponentsToPromises(
        renderProps.components, renderProps.params);

      Promise.all(promises).then((values) => {
        const data = prepareData(values, components);

        res.render('admin/index', {
          context: data,
        });
      }).catch((reason) => {
        console.log(reason);
        res.status(500).send(reason);
      });
    });
  });
};

const login = (router) => {
  router.get('/login', (req, res) => {
    res.render('admin/login.pug');
  });
};

const authentificate = (router) => {
  router.post('/login',
    passport.authenticate('local',
      { successRedirect: '/admin', failureRedirect: '/admin/login' })
  );
};

export default [home, login, authentificate];
