import { renderToString } from 'react-dom/server';
import { RouterContext } from 'react-router';
import ContextWrapper from '../../../../Components/common/ContextWrapper';

const render = (renderProps, data) => {
  return renderToString(
    <ContextWrapper data={data}>
      <RouterContext {...renderProps} />
    </ContextWrapper>
  );
};

export default render;
