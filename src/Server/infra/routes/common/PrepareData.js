const PrepareData = (values, components) => {
  const map = {};

  values.forEach((value) => {
    map[components[0].NAME] = Object.assign({}, map[components[0].NAME], value.data);
  });

  return map;
};

export default PrepareData;
