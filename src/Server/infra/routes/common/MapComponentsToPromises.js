import nconf from 'nconf';

const MapComponentsToPromises = (components, params) => {
  const filteredComponents = components.filter(Component =>
    typeof Component.requestData === 'function'
  );

  const promises = [].concat.apply([], filteredComponents.map(Component =>
    Component.requestData(params, nconf.get('domain'))
  ));
  return { promises, components: filteredComponents };
};

export default MapComponentsToPromises;
