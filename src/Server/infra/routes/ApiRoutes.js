import Listing from '../../Models/Listing';
import Broker from '../../Models/Broker';
import Regions from '../../Models/Regions';

const getListings = (router) => {
  router.get('/listings', (req, res) => {
    const l = Listing.find({}).limit(3);
    l.exec((err, listings) => {
      if (err) {
        res.status(500).send(err);
        return;
      }
      res.json({ listings });
    });
  });
};

const getEditFormInfo = (router) => {
  router.get('/edit/:id?', (req, res) => {
    const promises = [
      Broker.find({}).sort({ fullName: 1 }).exec(),
      Regions.find({}).sort({ label: 1 }).exec(),
    ];
    const id = req.params.id;
    if (id) {
      promises.push(Listing.findById(req.params.id).exec());
    }
    Promise.all(promises).then(
      values => res.json({
        brokers: values[0],
        regions: values[1],
        listing: (id) ? values[2] : new Listing(),
      }),
      error => res.status(500).send(error)
    );
  });
};

const saveListing = (router) => {
  router.put('/listing/:id?', (req, res) => {
    const payload = req.body;
    if (!req.params.id) {
      delete payload._id;
      Listing.create(payload, err => {
        if (err) {
          res.status(500).send(err);
          return;
        }
        res.sendStatus(200);
      });
    } else {
      Listing.update({ _id: req.params.id }, { $set: payload }, (err) => {
        if (err) {
          res.status(500).send(err);
          return;
        }
        res.sendStatus(200);
      });
    }
  });
};

const deleteListing = (router) => {
  router.delete('/listing/:id', (req, res) => {
    Listing.remove({ _id: req.params.id }, (err) => {
      if (err) {
        res.status(500).send(err);
        return;
      }
      res.sendStatus(200);
    });
  });
};

const getListingId = (router) => {
  router.get('/listing/:id?', (req, res) => {
    Listing.findById(req.params.id, (err, listing) => {
      if (err) {
        res.status(500).send(err);
      }
      res.json({ listing });
    });
  });
};

const viewById = (router) => {
  router.get('/view/:id', (req, res) => {
    Listing.findById(req.params.id)
      .populate('broker', 'fullName image')
      .populate('location.region', 'label')
      .sort({
        active: 'desc',
        hot: 'asc',
        created: 'desc',
      })
      .exec((err, listing) => {
        if (err) {
          res.status(500).send(err);
        }
        res.json({ listing });
      });
  });
};

const getBrokers = (router) => {
  router.get('/brokers', (req, res) => {
    Broker.find({}).sort({ fullName: 1 }).exec((err, brokers) => {
      if (err) {
        res.status(500).send(err);
        return;
      }
      res.json({ brokers });
    });
  });
};

const getRegions = (router) => {
  router.get('/regions', (req, res) => {
    Regions.find({}).sort({ label: 1 }).exec((err, regions) => {
      if (err) {
        res.status(500).send(err);
        return;
      }
      res.json({ regions });
    });
  });
};

const createAdminList = (router) => {
  router.get('/admin/list', (req, res) => {
    const fields = [
      'location', 'hot', 'active', 'broker', 'title',
      'price', 'sale', 'size', 'type',
    ];

    Listing.find().
      select(fields.join(' ')).
      populate('broker', 'fullName').
      sort({ active: 'desc', hot: 'asc', created: 'desc' }).
      exec((err, listings) => {
        if (err) {
          res.status(500).send(err);
        }
        res.json({ listings });
      });
  });
};

export default [
  createAdminList,
  getListingId,
  getBrokers,
  getListings,
  getRegions,
  saveListing,
  deleteListing,
  getEditFormInfo,
  viewById,
];
