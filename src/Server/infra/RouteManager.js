import { Router } from 'express';
import baseManager from './BaseManager';
import apiRoutes from './routes/ApiRoutes';
import adminRoutes from './routes/AdminRoutes';
import userRoutes from './routes/UserRoutes';
import subdomain from 'express-subdomain';

const createRouter = (routes) => {
  const router = new Router();
  routes.forEach(route => route(router));

  return router;
};

const routeManager = Object.assign({}, baseManager, {
  configureCommon(app) {
    app.use('/admin', createRouter(adminRoutes));
    app.use('/api', createRouter(apiRoutes));
    app.use('/', createRouter(userRoutes));
  },
});

export default routeManager;
