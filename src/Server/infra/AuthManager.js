import passport from 'passport';
import { Strategy } from 'passport-local';
import baseManager from './BaseManager';
import User from '../Models/Users';

const AuthManager = Object.assign({}, baseManager, {

  configureCommon(app) {
    app.use(passport.initialize());
    app.use(passport.session());
    passport.use(new Strategy(User.authenticate()));
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());
  },
});

export default AuthManager;