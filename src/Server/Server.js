import express from 'express';
import nconf from 'nconf';
import configManager from './infra/ConfigManager';
import middlewareManager from './infra/MiddlewareManager';
import routeManager from './infra/RouteManager';
import assetsManager from './infra/AssetsManager';
import databaseManager from './infra/DatabaseManager';
import authManager from './infra/AuthManager';

const app = express();

configManager.handle(app);
middlewareManager.handle(app);
authManager.handle(app);
assetsManager.handle(app);
routeManager.handle(app);
databaseManager.handle(app);

app.listen(nconf.get('port'));
