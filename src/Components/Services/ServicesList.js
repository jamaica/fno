import "./services.styl";

import React from 'react';
import { Row } from 'react-bootstrap';

import services from '../data/services.json';
import ServiceTile from './ServiceTile.js';

export default class ServiceList extends React.Component {

  constructor(props) {
    super(props);

    this.serviceIconMapping = {
      'find-places': 'icon-location-pin',
      'experienced-agents': 'icon-people',
      'beautiful-properties': 'icon-home',
      'agent-account': 'icon-cloud-upload'
    };
  }

  renderTiles() {
    return services.data.map((service, index) => {
      let icon = this.serviceIconMapping[service.id];

      return (
        <ServiceTile
          key={index}
          icon={icon}
          title={service.title}
          description={service.description}>
        </ServiceTile>
      );
    })
  }

  render() {
    return (
      <div className="home-content">
        <h2 className="osLight">Our Services</h2>
        <Row className="pb40">
          {this.renderTiles()}
        </Row>
      </div>
    );
  }
}