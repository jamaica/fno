import "./button-dropdown.styl";

import React from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';

export default class ButtonDropdown extends React.Component {

  state = {
    selectedOption: {}
  }

  handleSelect(e, key) {
    this.setState({ selectedOption: this.props.options.find(option => option.key === key) });
  }

  getValue() {
    return this.state.selectedOption.key;
  }

  renderOptions() {
    return this.props.options.map((option, index) => {
      var active = option.key === this.state.selectedOption.key;
      return (
        <MenuItem
          key={index}
          eventKey={option.key}
          active={active}
          value={option.value}
          onSelect={this.handleSelect.bind(this)}
        >
          {option.value}
        </MenuItem>
      )
    });
  }

  render() {
    return (
      <DropdownButton {...this.props} title={this.state.selectedOption.value || this.props.placeholder}>
        {this.renderOptions()}
      </DropdownButton>
    )
  }
}