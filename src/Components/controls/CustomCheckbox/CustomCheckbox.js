import "./custom-checkbox.styl";

import React from 'react';

export default class CustomCheckbox extends React.Component {

  getValue() {
    return this._input.checked;
  }

  render() {
    var groupClassName = 'form-group' + (this.props.groupClassName ? ' ' + this.props.groupClassName : '');

    return (
      <div className={groupClassName}>
        <div className="checkbox custom-checkbox">
          <label>
            <input type="checkbox" ref={(ref) => this._input = ref}/>
            <span className="checkbox-replacement fa fa-check"></span>
            <span>{this.props.label}</span>
          </label>
        </div>
      </div>
    )
  }
}