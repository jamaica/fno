import React from 'react';
import { GoogleMap } from 'react-google-maps';
import { default as FaSpinner } from 'react-icons/lib/fa/spinner';
import { default as ScriptjsLoader } from 'react-google-maps/lib/async/ScriptjsLoader';

export default class MapAutocomplete extends React.Component {

  constructor(props) {
    super(props);

    this.options = {
      types: ['geocode'],
      componentRestrictions: {
        country: 'US'
      }
    }
  }

  getValue() {
    return this._input.value;
  }

  componentDidMount() {
    //new google.maps.places.Autocomplete(this._input, this.options);
  }

  render() {
    return (
      <input {...this.props} ref={(ref) => this._input = ref} type="text"/>
    )
  }
}