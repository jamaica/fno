import "./info-banner.styl";

import React from 'react';

export default class InfoBanner extends React.Component {

  render() {
    return (
      <div className="highlight">
        <div className="h-title osLight">Find your new place</div>
        <div className="h-text osLight">Fusce risus metus, placerat in consectetur eu, porttitor a est sed sed dolor
          lorem cras adipiscing
        </div>
      </div>
    )
  }
}