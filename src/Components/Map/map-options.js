export default {
  defaultZoom: 12,
  defaultLocation: {
    lat: 40.77,
    lng: -73.95,
  },
  general: {
    styles: [{
      stylers: [{
        hue: '#ccc',
      }, {
        saturation: -100,
      }],
    }, {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [{
        lightness: 100,
      }, {
        visibility: 'simplified',
      }],
    }, {
      featureType: 'road',
      elementType: 'labels',
      stylers: [{
        visibility: 'on',
      }],
    }, {
      featureType: 'poi',
      stylers: [{
        visibility: 'off',
      }],
    }],
    disableDefaultUI: true,
    scrollwheel: false,
    mapTypeControlOptions: {
      mapTypeIds: ['Styled'],
    },
  },
};
