import React, { PropTypes } from 'react'

import OfficeMarker from './OfficeMarker';
import OfficeInfoBox from './OfficeInfoBox';

export default class MarkerList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      markers: [],
      mapHolderRef: props.mapHolderRef,
      showInfoBox: props.showInfoBox || false,
      draggable: props.draggable || false
    };
  }

  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
    const markers = data.map(marker => {
      return Object.assign({}, marker, { visible: false });
    });

    this.setState({ markers });
  }

  showInfoBox(index) {
    let { markers } = this.state;
    markers.forEach((marker, i) => {
      marker.visible = (i === index);
    });
    this.setState({ markers });
  };

  closeInfoBox(index) {
    let { markers } = this.state;
    let marker = markers[index];
    marker.visible = false;
    this.setState({ markers });
  };

  render() {
    const { markers, mapHolderRef, showInfoBox, draggable } = this.state;
    const { getCoords } = this.props;
    return (
      <div>
        {markers.map((listing, index) => {
          let position = { lat: listing.location.lat, lng: listing.location.lng };
          return (
            [
              <OfficeMarker
                draggable={draggable}
                getCoords={getCoords}
                mapHolderRef={mapHolderRef}
                position={position}
                onClick={this.showInfoBox.bind(this, index)}>
              </OfficeMarker>,
              listing.visible && showInfoBox ? <OfficeInfoBox
                defaultPosition={position}
                info={listing}
                onClose={this.closeInfoBox.bind(this, index)}>
              </OfficeInfoBox> : null
            ]
          )

        })}
      </div>
    )
  }
}

MarkerList.propTypes = {
  position: PropTypes.shape({
    lat: PropTypes.number,
    lnt: PropTypes.number
  })
};

export default MarkerList;