import './office-infobox.styl';
import { default as React, PropTypes } from 'react';
import { default as InfoBox } from 'react-google-maps/lib/addons/InfoBox';
import * as StringFormatter from './../utils/StringFormatter';
import { infoBoxBackground } from '../images/images';

export default class OfficeInfoBox extends React.Component {

  constructor() {
    super();
    const { Size } = google.maps;
    this.options = {
      visible: false,
      maxWidth: 202,
      pixelOffset: new Size(-101, -285),
      boxStyle: {
        background: `url(${infoBoxBackground})  no-repeat`,
        opacity: 1,
        width: '202px',
        height: '220px',
      },
      closeBoxMargin: '28px 26px 0px 0px',
      closeBoxURL: '',
      infoBoxClearance: new Size(1, 1),
      enableEventPropagation: true,
    };
  }

  render() {
    const { marker, onClose } = this.props;
    const { LatLng } = google.maps;
    const { location, price, size, visible, type, sale, description, images } = marker;
    const { lat, lng, address } = location;
    const imageSrc = 'data:image;base64,' + images[0];
    const formattedPrice = StringFormatter.toMoney(price);
    const formattedSquare = StringFormatter.toSquareMetres(size);
    const defaultPosition = new LatLng(lat, lng);
    this.options.visible = visible;

    return (
      <InfoBox
        {...this.props}
        defaultPosition={defaultPosition}
        options={this.options}
      >
        <div className="infoW">
          <div className="propImg">
            <img src={imageSrc} alt="Office View" />
            <div className="propBg">
              <div className="propPrice">{formattedPrice}</div>
              <div className="propType">{type}</div>
              <div className="propSale">{sale}</div>
            </div>
          </div>
          <div className="paWrapper">
            <div className="propTitle">{address}</div>
            <div className="propDescription">{description}</div>
          </div>
          <ul className="propFeat">
            <li className="square"><span className="icon-frame"></span>{' '}{formattedSquare}</li>
          </ul>
          <div className="clearfix"></div>
          <div className="infoButtons">
            <a className="btn btn-sm btn-round btn-gray btn-o closeInfo" onClick={onClose}>Close</a>
            <a href="javascript:void(0);" className="btn btn-sm btn-round btn-green viewInfo">View</a>
          </div>
        </div>
      </InfoBox>
    );
  }
}

OfficeInfoBox.propTypes = {
  onClose: PropTypes.func.isRequired,
  marker: PropTypes.shape({
    _id: PropTypes.string,
    active: PropTypes.bool.isRequired,
    broker: PropTypes.string.isRequired,
    description: PropTypes.string,
    images: PropTypes.array,
    location: PropTypes.shape({
      lat: PropTypes.number.isRequired,
      lng: PropTypes.number.isRequired,
    }),
    price: PropTypes.number.isRequired,
    sale: PropTypes.string.isRequired,
    size: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
};
