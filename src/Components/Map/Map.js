import './map.styl';
import { default as React, Component, PropTypes } from 'react';
import { GoogleMap } from 'react-google-maps';
import { default as FaSpinner } from 'react-icons/lib/fa/spinner';
import { default as ScriptjsLoader } from 'react-google-maps/lib/async/ScriptjsLoader';
import Marker from './OfficeMarker';
import OfficeInfoBox from './OfficeInfoBox';
import mapOptions from './map-options';
import update from 'react-addons-update';

export default class Map extends Component {

  constructor(props) {
    super(props);
    const { markers = [] } = props;
    const state = {
      markers,
    };

    this.state = state;
  }

  componentWillReceiveProps(nextProps) {
    const markers = nextProps.markers.map(marker =>
      Object.assign({}, marker, { visible: false })
    );

    this.setState({ markers });
  }

  toogleInfoBox(index) {
    const markers = update(this.state.markers, {
      [index]: { visible: { $apply: visible => !visible } },
    });

    this.setState({ markers });
  }

  renderMarkers() {
    const { markers } = this.state;

    return markers.map((marker, index) => {
      const { lat, lng } = marker.location;
      const position = { lat, lng };
      const key = `marker_${index}`;
      const toggle = this.toogleInfoBox.bind(this, index);
      const markerOptions = Object.assign({}, marker.options, { key, position, onClick: toggle });
      const infoBoxOptions = Object.assign({}, { marker, onClose: toggle });
      return [
        <Marker {...markerOptions} />,
        marker.visible && marker.options.infoBox ?
          <OfficeInfoBox {...infoBoxOptions} /> :
          null,
      ];
    });
  }

  render() {
    const { id } = this.props;

    return (
      <ScriptjsLoader
        hostname={"maps.googleapis.com"}
        pathname={"/maps/api/js"}
        query={{ v: '3.24', key: 'AIzaSyCAIOB8_QiwLwggTc88_Y5568lwtQrPZ9E' }}
        loadingElement={
          <div style={{ height: '100%' }}>
            <FaSpinner
              style={{
                display: 'block',
                width: 200,
                height: 200,
                margin: '100px auto',
                animation: 'fa-spin 2s infinite linear',
              }}
            />
          </div>
        }
        containerElement={
          <div id={id} style={{ height: '100%' }} />
        }
        googleMapElement={
          <GoogleMap
            defaultZoom={mapOptions.defaultZoom}
            defaultCenter={mapOptions.defaultLocation}
            defaultOptions={mapOptions.general}
          >
            {this.renderMarkers()}
          </GoogleMap>
        }
      />
    );
  }
}

Map.propTypes = {
  id: PropTypes.string.isRequired,
  markers: PropTypes.arrayOf(PropTypes.shape({
    _id: PropTypes.string,
    active: PropTypes.bool,
    broker: PropTypes.string,
    description: PropTypes.string,
    images: PropTypes.array,
    location: PropTypes.shape({
      lat: PropTypes.number,
      lng: PropTypes.number,
    }),
    price: PropTypes.number,
    sale: PropTypes.string,
    size: PropTypes.number,
    title: PropTypes.string,
    type: PropTypes.string,
  })).isRequired,
};
