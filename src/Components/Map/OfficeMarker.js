import { default as React, PropTypes } from 'react';
import { Marker } from 'react-google-maps';

export default class OfficeMarker extends React.Component {

  shouldComponentUpdate(nextProps) {
    const next = nextProps.position;
    const current = this.props.position;
    return next.lat !== current.lat || next.lng !== current.lng;
  }

  render() {
    const options = {
      animation: 2,
    };

    return (
      <Marker {...this.props} {...options} />
    );
  }
}

OfficeMarker.propTypes = {
  position: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
  }).isRequired,
  icon: PropTypes.string.isRequired,
};
