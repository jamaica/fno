import React from 'react';
import Form from "react-jsonschema-form";
import UploadImageBlock from '../UploadImageBlock/UploadImageBlock.js';
import Dropdown from '../Dropdown/Dropdown.js';

import './editForm.styl';

export default class EditForm extends React.Component {

  constructor() {
    super();

    this.state = {};
  }

  componentWillMount() {
    const item = Object.assign({}, this.props.item);
    this.setState(item);
    this.setState({
      validates: {
        price: true
      }
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.coords) {
      this.setState({
        location: nextProps.coords
      });
      return true;
    }
  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  validate(type, value) {
    var error;
    switch (type) {
      case 'price':
        if (!this.isNumeric(value)) {
          $(this._price).closest('.form-group').addClass('has-error');
          error = {
            price: false
          };
        } else {
          $(this._price).closest('.form-group').removeClass('has-error');
          error = {
            price: true
          };
        }
        break;
      case 'size':
        if (!this.isNumeric(value)) {
          $(this._size).closest('.form-group').addClass('has-error');
          error = {
            size: false
          };
        } else {
          $(this._size).closest('.form-group').removeClass('has-error');
          error = {
            size: true
          };
        }
        break;
    }
    this.setState({
      validates: $.extend(this.state.validates, error)
    });
  }

  renderPrice() {
    const price = this.state.price;
    return (
      <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div className="form-group">
          <label>Price</label>
          <div className="input-group">
            <div className="input-group-addon">$</div>
            <input type="text"
                   className="form-control"
                   defaultValue={price}
                   onChange={(event)=> {
                     this.validate('price', event.target.value);
                   }}
                   ref={c => this._price = c}/>
          </div>
        </div>
      </div>
    )
  }

  renderDescription() {
    const description = this.state.description;
    return (
      <div className="form-group">
        <label>Description</label>
        <textarea className="form-control"
                  rows="4"
                  defaultValue={description}
                  ref={c => this._description = c}>
                </textarea>
      </div>
    )
  }

  renderAddress() {
    const address = this.state.location && this.state.location.address;
    const lat = this.state.location && this.state.location.lat;
    const lng = this.state.location && this.state.location.lng;

    return (
      <div className="form-group">
        <label>Address
          <span id="latitude" className="label label-default">123123123</span>
          <span id="longitude" className="label label-default">123123123</span>
        </label>
        <input className="form-control"
               type="text"
               id="address"
               placeholder="Enter a Location"
               autoComplete="off"
               onChange={(e) => this.setState({ addressValue: e.target.value }) }
               value={this.state.addressValue || address}
               ref={c => this._address = c}
        />
        <p className="help-block">You can drag the marker to property position</p>
      </div>
    )
  }

  renderSize() {
    const size = this.state.size;
    return (
      <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <div className="form-group">
          <label>Size</label>
          <div className="input-group">
            <input type="text"
                   className="form-control"
                   defaultValue={size}
                   onChange={(event)=> {
                     this.validate('size', event.target.value);
                   }}
                   ref={c => this._size = c}/>
          </div>
        </div>
      </div>
    )
  }

  renderType() {
    return (
      <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <Dropdown title='Type'
                  active={this.state.type}
                  list={[{ fullName: this.state.type, id: this.state.type }]}
                  onSelect={(activeId)=> {
                    this.setState({
                      type: activeId
                    });
                  }}/>
      </div>
    )
  }

  renderSale() {
    return (
      <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <Dropdown title='Sale'
                  active={this.state.sale}
                  list={[{ fullName: this.state.sale, id: this.state.sale }]}
                  onSelect={(activeId)=> {
                    this.setState({
                      type: activeId
                    });
                  }}/>
      </div>
    )
  }

  renderRegions() {
    const location = this.state.location;
    return (
      <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <Dropdown title='Regions'
                  active={location && location.region}
                  list={location && [{ fullName: location.region, id: location.region }]}
        />
      </div>
    )
  }

  renderBrokers() {
    return (
      <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <Dropdown title='Brokers'
                  active={this.state.broker}
                  list={this.props && this.props.brokers}
                  onSelect={(activeId)=> {
                    this.setState({
                      broker: activeId
                    });
                  }}/>
      </div>
    )
  }

  renderStatus() {
    return (
      <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <Dropdown title='Status'
                  active={+this.state.active}
                  list={[{ fullName: String('true'), id: 1 },
                    { fullName: String('false'), id: 0 }]}
                  onSelect={(activeId)=> {
                    this.setState({
                      active: Boolean(activeId)
                    });
                  }}/>
      </div>
    )
  }

  renderUploadButton() {
    return (
      <div className="form-group">
        <a href="#" className="btn btn-green btn-lg" onClick={this.getData.bind(this)}>Add Property</a>
      </div>
    )
  }

  getImagesSrc(imagesSrc) {
    this.setState({
      images: imagesSrc
    })
  }

  getData() {
    for (var i in this.state.validates) {
      if (!this.state.validates[i]) {
        alert('please check you data');
        return;
      }
    }

    let data = {
      title: this._title.value,
      description: this._description.value,
      price: this._price.value,
      size: this._size.value,
      active: this.state.active,
      broker: this.state.broker,
      images: this.state.images,
      location: this.state.location,
      sale: this.state.sale,
      type: this.state.type
    };

    this.setState(data);

    alert(JSON.stringify(data), 'data');
  }

  render() {
    const { data } = this.props;
    const log = (type) => console.log.bind(console, type);

    return (
      <div className='editForm'>
        <Form schema={schema}
              uiSchema={uiSchema}
              fields={fields}
              formData={data}
              onChange={log("changed")}
              onSubmit={log("submitted")}
              onError={log("errors")}/>
        <h1>List a New Property</h1>
        <form>
          <div className='row'>
            {this.renderTitle()}
            {this.renderPrice()}
          </div>
          {this.renderDescription()}
          {this.renderAddress()}
          <div className="row form-group">
            {this.renderType()}
            {this.renderBrokers()}
            {this.renderRegions()}
            {this.renderStatus()}
          </div>
          <div className="row">
            {this.renderSize()}
            {this.renderSale()}
          </div>
          <div className="row">
            <UploadImageBlock
              images={this.state.images}
              getResult={this.getImagesSrc.bind(this)}
            />
          </div>
          {this.renderUploadButton()}
        </form>
      </div>
    )
  }

  renderTitle() {
    let title = this.state.title;
    return (
      <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <div className="form-group">
          <label>Title</label>
          <input type="text"
                 className="form-control"
                 defaultValue={title}
                 ref={c => this._title = c}/>
        </div>
      </div>
    )
  }
}











