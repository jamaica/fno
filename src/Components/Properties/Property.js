import React from 'react';
import { Col } from 'react-bootstrap';
import { Link } from 'react-router';
import * as StringFormatter from '../utils/StringFormatter';

export default class Property extends React.Component {


  renderImage(image) {
    const imageSrc = "data:image;base64," + image;
    return image ?
      [
        <img key={1} className="property" src={imageSrc} alt="property" />,
        <img key={2} className="property blur" src={imageSrc} alt="property" />
      ] :
      [
        <div key={1} className="property"><i className="icon-home s-icon" /></div>,
        <div key={2} className="property blur"><i className="icon-home s-icon" /></div>
      ];
  }

  render() {
    const data = this.props;
    const formattedPrice = StringFormatter.toMoney(data.price);
    const formattedSquare = StringFormatter.toSquareMetres(data.size);
    const id = `/view/${data._id}`;
    return (
      <Col xs={12} sm={6} md={4} lg={4}>
        <Link to={id} className="propWidget-2">
          <div className="fig">
            {this.renderImage(data.images[0])}
            <div className="opac" />
            <div className="priceCap osLight">
              <span>{formattedPrice}</span>
            </div>
            <div className="figType">{data.type}</div>
            <div className="osLight description">{data.description}</div>
            <div className="address">{data.region}</div>
            <div className="square">{formattedSquare}</div>
          </div>
        </Link>
      </Col>
    )
  }
}