import React from 'react';
import { Row } from 'react-bootstrap';
import "./properties.styl";
import Property from './Property.js';

export default class PropertiesList extends React.Component {

  renderProperties() {
    return this.props.properties.map((property, index) => {
      return (
        <Property {...property} key={index} />
      );
    })
  }

  render() {
    return (
      <div className="home-content">
        <h2 className="osLight">Recently Listed Properties</h2>
        <Row className="pb40">
          {this.renderProperties()}
        </Row>
      </div>
    );
  }
}