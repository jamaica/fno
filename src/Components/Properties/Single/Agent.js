import React, { Component } from 'react';
import './agent.styl';

const Agent = ({ data }) =>  {

    return (
      <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <div className="agent-owner">
          <div className="clearfix" />
          <img className="avatar" src={data.image} />
          <div className="name">{data.fullName}</div>
          <a data-toggle="modal" href="#contactAgent" className="btn btn-lg btn-round btn-green contact">
            Contact Agent
          </a>
        </div>
      </div>
    );
}

export default Agent;
