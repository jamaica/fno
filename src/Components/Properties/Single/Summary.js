import React, { Component } from 'react';
import Agent from './Agent';
import './summary.styl';

class Summary extends Component {

  render() {
    const { broker } = this.props;
    return (
      <div className="summary">
        <div className="row">
          <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div className="summaryItem">
              <h1 className="pageTitle">Modern Residence in New York</h1>
              <div className="address">
                <span className="icon-location-pin"/> 39 Remsen St, Brooklyn, NY 11201, USA
              </div>
              <div className="clearfix"></div>
              <ul className="features">
                <li><span className="fa fa-moon-o"></span>
                  <div>2 Bedrooms</div>
                </li>
                <li><span className="icon-drop"></span>
                  <div>2 Bathrooms</div>
                </li>
                <li><span className="icon-frame"></span>
                  <div>2750 Sq Ft</div>
                </li>
              </ul>
              <div className="clearfix"></div>
            </div>
          </div>
          <Agent data={broker} />
        </div>
      </div>
    );
  }
}

export default Summary;
