import { default as React, Component, PropTypes } from 'react';
import ReactCrop from 'react-image-crop';
import 'css!react-image-crop/dist/ReactCrop.css';
import './imageElemet.styl';

class ImageRow extends Component {

  constructor(props) {
    super(props);
    const { image = {}, handleChange, removeImage, index } = props;
    this.state = {
      original: image,
      preview: '',
    };
    this.onComplete = this.onComplete.bind(this);
    this.cropAfterLoad = this.cropAfterLoad.bind(this);
    this.handleChange = handleChange;
    this.remove = removeImage;
  }

  onComplete(crop) {
    this.loadImage(crop, this.cropAfterLoad);
  }

  loadImage(crop, callback) {
    let image = new Image();
    image.onload = e => {
      callback(crop, image);
      image = null;
    };
    image.src = this.state.original;
  }

  cropAfterLoad(crop, loadedImg) {
    const imageWidth = loadedImg.naturalWidth;
    const imageHeight = loadedImg.naturalHeight;

    const cropX = (crop.x / 100) * imageWidth;
    const cropY = (crop.y / 100) * imageHeight;

    const cropWidth = (crop.width / 100) * imageWidth;
    const cropHeight = (crop.height / 100) * imageHeight;

    const canvas = document.createElement('canvas');
    canvas.width = cropWidth;
    canvas.height = cropHeight;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(loadedImg, cropX, cropY, cropWidth, cropHeight, 0, 0, cropWidth, cropHeight);
    const out = canvas.toDataURL('image/png');
    this.setState({ preview: out });
    this.handleChange(out);
  }

  render() {
    const { original, preview } = this.state;

    const options = {
      onComplete: this.onComplete,
      onImageLoaded: this.onComplete,
      crop: { width: 2000, aspect: 5 / 3 },
      src: original,
      keepSelection: true,
    };

    return (
      <div className="admin-image-galery container-fluid">
        <div className="row image-row">
          <button onClick={this.remove} type="button" className="btn-remove btn btn-default btn-lg btn-block">Remove</button>
          <div className="image col-xs-12 col-lg-6">
            <ReactCrop {...options} />
          </div>
          <div className="image col-xs-12 col-lg-6">
            {preview.length !== 0 ?
              <img alt="preview" className="img-responsive" src={preview}/> : null}
          </div>
        </div>
      </div>
    );
  }
}

export default ImageRow;

ImageRow.propTypes = {
  handleChange: PropTypes.func.isRequired,
  image: PropTypes.string.isRequired,
};
