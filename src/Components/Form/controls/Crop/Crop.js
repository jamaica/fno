import './crop.styl';

export default class Crop extends React.Component {

    constructor(){
        super();

        function Selection(x, y, w, h){
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.maxX = 0;
            this.maxY = 0;
            this.minX = -this.w;
            this.minY = -this.h;
        }

        Selection.prototype.draw = function(ctx, image){
            this.x = Math.max(Math.min(this.maxX, this.x), this.minX);
            this.y = Math.max(Math.min(this.maxY, this.y), this.minY);
            if (this.w > 0 && this.h > 0) {
                ctx.drawImage(image, 0, 0, this.w, this.h, this.x, this.y, this.w, this.h);
            }
        };

        this.state = {
            height: 350,
            width: 350,
            selection: new Selection(0, 0, 350, 350),
            needCrop: false
        };
    }

    componentWillMount(){
        this.setSizeWindow();
        this.prepareImage((needsCrop) => {
            this.setState({
                needsCrop: needsCrop
            });
            if (!this.state.needsCrop) {
                return;
            }
            this.drawCanvas();
            $('body').on('mousemove.'+this.props.id, this.onMoveSelection.bind(this));
            $('body').on('mouseup.'+this.props.id, this.onUpSelection.bind(this));
        });
    }


    prepareImage(callback){
        var that = this;
        this.image = new Image();
        this.image.src = this.props.src || '';

        this.image.onload = ()=>{
            if (this.image.width < this.state.width &&
                this.image.height < this.state.height
            ) {
                callback(false);
            } else {
                this.state.width = Math.min(this.state.width, this.image.width);
                this.state.height = Math.min(this.state.height, this.image.height);

                this.state.selection.w = this.image.width;
                this.state.selection.h = this.image.height;
                this.state.selection.minX = -(this.image.width - this.state.width);
                this.state.selection.minY = -(this.image.height - this.state.height);

                callback(true);
            }
        }
    }

    setSizeWindow(){
        $(this._selectBlock).height(this.props.height || this.state.height)
            .width(this.props.width || this.state.width);
    }

    drawCanvas(){
        var canvas = this._canvas;

        this.ctx = canvas.getContext('2d');
        this.ctx.canvas.width = this.state.width;
        this.ctx.canvas.height = this.state.height;

        this.drawImage();
    }

    redraw(){
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.drawImage();
    }

    drawImage(){
        this.state.selection.draw(this.ctx, this.image);
    }

    getResultImage(){
        const selection = this.state.selection;
        const tmpCanvas = document.createElement('canvas');
        const tmpCtx = tmpCanvas.getContext('2d');

        tmpCanvas.width = this.state.width;
        tmpCanvas.height = this.state.height;

        tmpCtx.drawImage(this.image, -selection.x, -selection.y, this.state.width, this.state.height, 0, 0, this.state.width, this.state.height);
        const vData = tmpCanvas.toDataURL();

        $(this._result).find('img').attr('src', vData);
        $(this._result).show();
        this.props.onCrop(vData);
    }

    onMoveSelection(e){
        if (!this.state.selection.bDragAll) {
            return;
        }

        this.state.selection.x += e.pageX - this.state.selection.px;
        this.state.selection.y += e.pageY - this.state.selection.py;
        this.state.selection.px = e.pageX;
        this.state.selection.py = e.pageY;

        this.redraw();

        e.preventDefault();
        e.stopPropagation();
    }

    onUpSelection(e){
        this.state.selection.bDragAll = false;
    }

    onDownSelection(e){
        this.state.selection.bDragAll = true;

        this.state.selection.px = e.pageX;
        this.state.selection.py = e.pageY;
    }

    componentWillUnmount(){
        $('body').off('mousemove.'+this.props.id);
        $('body').off('mouseup.'+this.props.id);
    }

    render() {
        return (
            this.state.needsCrop ?
                <div className="crop">
                    <div className="close crop__remove text-right" onClick={this.props.remove}>×</div>
                    <div className="row">
                        <div className="col-xs-6">
                            <div>drag it to crop</div>
                            <canvas className="crop__region" ref={(c) => this._canvas = c}
                                    onMouseDown={this.onDownSelection.bind(this)}
                                    onMouseUp={this.onUpSelection.bind(this)}>
                            </canvas>
                        </div>

                        <div className="col-xs-6">
                            <div className="row">
                                <div className='crop__result' ref={(c) => this._result = c}>
                                    <img/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-group">
                            <div className="btn btn-o btn-default crop__do-it"
                                 onClick={this.getResultImage.bind(this)}>Crop it!</div>
                        </div>
                    </div>
                </div>
                :
                <div className="nocrop">
                    <div className="close crop__remove text-right" onClick={this.props.remove}>×</div>
                    <img src={this.props.src}/>
                </div>
        )
    }
}