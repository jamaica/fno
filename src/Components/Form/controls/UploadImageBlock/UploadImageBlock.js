import React from 'react';
import * as DataURIGenerator from '../../../utils/DataURIGenerator';
import Crop from '../Crop/Crop';

import './UploadImageBlock.styl';

export default class UploadImageBlock extends React.Component {

    constructor(){
      super();

      this.state = {
          cropList: [],
          index: 0
      };
    }

    componentWillMount(){
        this.props && this.props.images && this.props.images.forEach(function(src, index) {
            this.setState({
                cropList: this.state.cropList.concat([{
                    dataUri: DataURIGenerator.generate(src),
                    id: 'crop'+this.state.index
                }]),
                index: ++this.state.index
            });
        }, this);
    }

    removeCrop(cropId){
        this.setState({
            cropList: this.state.cropList.filter(crop => {
                return (cropId != crop.id);
            })
        });
    }

    onCrop(cropId, cropDataUri) {
        this.setState({
            cropList: this.state.cropList.map((cropData)=>{
                if (cropId === cropData.id) {
                    cropData.dataUri = cropDataUri;
                }
                return cropData;
            })
        });

        const imagesSrcList = this.state.cropList.map(function(cropsData){
            return cropsData.dataUri;
        });

        this.props.getResult(imagesSrcList);
    }

    readFile(e) {
        e.preventDefault();
        var that = this;

        var $input = $('<input type="file" id="file" multiple accept="image/gif, image/jpeg, image/png"/>')
            .css({
                visibility: 'hidden',
                position: 'absolute'
            })
            .prependTo($('body'))
            .change(function(){
                var files = Array.prototype.slice.call(this.files);
                files.forEach(function(file, index) {
                    if (file instanceof File) {
                        var reader = new FileReader;
                        reader.onload = function(e) {
                            that.setState({
                                cropList: that.state.cropList.concat([{
                                        dataUri: e.target.result,
                                        id: 'crop' + that.state.index
                                }]),
                                index: ++that.state.index
                            })
                        };
                        reader.readAsDataURL(file);
                    }
                });
            });
        $input[0]
            .click();
    }

    render() {
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="form-group">
                    <label>Image Gallery</label>
                    <div className="file-input file-input-new">
                        <div className="file-preview ">
                            <div className="file-preview-thumbnails">
                                {this.state.cropList.map((crop, index)=>{
                                    return (<Crop src={crop.dataUri}
                                                  id={crop.id}
                                                  onCrop={this.onCrop.bind(this, crop.id)}
                                                  remove={this.removeCrop.bind(this, crop.id)}
                                                  key={crop.id}/>)
                                })}
                            </div>
                            <div className="clearfix"></div>
                        </div>

                        <div onClick={this.readFile.bind(this)} className="btn btn-o btn-default btn-file">
                            <i className="glyphicon glyphicon-folder-open"></i>
                            &nbsp;Browse Images
                        </div>
                    </div>
                    <p className="help-block">You can select multiple images at once</p>
                </div>
            </div>
        )
    }
}