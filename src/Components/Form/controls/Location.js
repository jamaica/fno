const Location = ({ formData, schema }) => {
  const { address, lat, lng, region } = formData;
  const { regionSchema } = schema.properties.region;
  console.log(schema.properties);
  return (
    <div>
      <label className="control-label">
        Address
        <span id="latitude" className="label label-default">{lat}</span>
        <span id="longitude" className="label label-default">{lng}</span>
      </label>
      <input className="form-control"
             type="text"
             id="address"
             placeholder="Enter a Location"
             autoComplete="off"
             value={address}
      />
      <p className="help-block">You can drag the marker to property position</p>
    </div>
  );
};

export default Location;