import { default as React, Component, PropTypes } from 'react';
import Dropzone from 'react-dropzone';
import Image from './ImageElement';

class Images extends Component {

  constructor(props) {
    super(props);
    const { formData, onChange } = props;
    this.state = { images: formData };
    this.onDrop = this.onDrop.bind(this);
    this.load = this.load.bind(this);
    this.onChange = onChange;
  }

  componentWillReceiveProps({ formData }) {
    this.setState({ images: formData });
  }

  onDrop(files) {
    files.forEach(file => {
      const reader = new FileReader();
      reader.addEventListener('load', e => this.load(e));
      reader.readAsDataURL(file);
    });
  }

  load(e) {
    const images = [...this.state.images];
    images.push(e.currentTarget.result);
    this.setState({ images });
    this.onChange(images);
  }

  handleChange(index, image) {
    const { images } = this.state;
    images[index] = image;
    this.setState({ images });
    this.onChange(images);
  }

  removeImage(index) {
    let images = [...this.state.images];
    images = images.filter((image, key) => key !== index);
    this.setState({ images }, () => this.onChange(this.state.images));
  }

  render() {
    const { images } = this.state;
    const croppers = images.map((image, index) => {
      const options = {
        key: `image_${index}`,
        index,
        image,
        removeImage: this.removeImage.bind(this, index),
        handleChange: this.handleChange.bind(this, index),
      };

      return <Image {...options} />;
    });

    return (
      <Dropzone ref={dropzone => { this.dropzone = dropzone; }} disableClick className="col-xs-12" onDrop={this.onDrop}>
          {croppers}
        <button onClick={() => this.dropzone.open()} type="button" className="btn-remove btn btn-default btn-lg btn-block">Upload</button>
      </Dropzone>
    );
  }
}

export default Images;

Images.propTypes = {
  onChange: PropTypes.func.isRequired,
  formData: PropTypes.array.isRequired,
};
