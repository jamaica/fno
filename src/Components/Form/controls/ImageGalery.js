import UploadImageBlock from "./UploadImageBlock/UploadImageBlock";

const ImageGalery = ({ formData }) => {
  /*getResult={this.getImagesSrc.bind(this)}
   * XXXX remove this ugly window hack */
  return (
    <div className="row">
      { (typeof window != 'undefined' && window.document) &&
      <UploadImageBlock
        images={formData}

      />}
    </div>
  );
}

export default ImageGalery;