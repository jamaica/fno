export default {
  title: {
    classNames: 'col-xs-12 col-sm-12 col-md-8 col-lg-8',
  },
  price: {
    classNames: 'col-xs-12 col-sm-12 col-md-4 col-lg-4',
  },
  description: {
    'ui:widget': 'textarea',
    classNames: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
  },
  _id: {
    'ui:widget': 'hidden',
  },
  location: {
    classNames: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
    lat: {
      'ui:widget': 'hidden',
    },
    lng: {
      'ui:widget': 'hidden',
    },
    /*'ui:field': 'location',*/
  },
  type: {
    classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
  },
  broker: {
    classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
  },
  size: {
    classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
  },
  sale: {
    classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
  },
  active: {
    classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
  },
  hot: {
    classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
  },
  images: {
    'ui:field': 'images',
  },
};
