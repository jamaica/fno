import './editForm.styl';
import { default as React, Component, PropTypes } from 'react';
import Images from './controls/Images';
import Form from 'react-jsonschema-form';

export default class ListingForm extends Component {

  constructor(props) {
    super(props);
    this.fields = { images: Images };
  }

  onError(a, b, c){
    console.log("Change", a,b,c);
  }

  render() {
    const { listing, onSubmit, onChange, schema, uiSchema } = this.props;

    const options = {
      schema,
      fields: this.fields,
      uiSchema,
      formData: listing,
      onSubmit,
      onChange,
      onError: this.onError,
    };

    return (
      <div className="editForm">
        <Form {...options} />
      </div>
    );
  }
}

ListingForm.propTypes = {
  listing: PropTypes.shape({
    _id: PropTypes.string,
    active: PropTypes.bool,
    broker: PropTypes.string,
    created: PropTypes.string,
    description: PropTypes.string,
    images: PropTypes.array,
    location: PropTypes.shape({
      lat: PropTypes.number,
      lng: PropTypes.number,
      region: PropTypes.string,
      address: PropTypes.string,
    }),
    price: PropTypes.number,
    sale: PropTypes.string,
    size: PropTypes.number,
    title: PropTypes.string,
    type: PropTypes.string,
  }),
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  schema: PropTypes.object.isRequired,
  uiSchema: PropTypes.object.isRequired,
};
