import React from 'react';
import { Col, FormControl } from 'react-bootstrap';

export default class SubscribeForm extends React.Component {

  render() {
    return (
      <Col xs={12} sm={6} md={3} lg={3}>
        <div className="osLight footer-header">Subscribe to Our Newsletter</div>
        <form role="form">
          <FormControl
            placeholder="Email Address"
            type="email"
          />
          <div className="form-group">
            <a href="javascript:void(0);" className="btn btn-green btn-block">Subscribe</a>
          </div>
        </form>
      </Col>
    )
  }

}