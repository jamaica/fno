import React from 'react';

export default class Copyright extends React.Component {

  render() {
    return (
      <div className="copyright">Find NY Office<br/> &copy; {new Date().getFullYear()}</div>
    )
  }
}