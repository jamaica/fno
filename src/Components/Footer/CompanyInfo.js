import React from 'react';
import { Col } from 'react-bootstrap';
import SocialButtons from './../common/SocialButtons/SocialButtons.js';

export default class CompanyInfo extends React.Component {

  contacts = {
    email: '',
    facebook: '',
    twitter: '',
    gplus: ''
  }

  render() {
    return (
      <Col xs={12} sm={6} md={3} lg={3}>
        <div className="osLight footer-header">Get in Touch</div>
        <ul className="footer-nav pb20">
          <li className="footer-phone">
            <span className="fa fa-phone"></span> 800 234 67 89
          </li>
          <li className="footer-address osLight">
            <p>516 Green St</p>
            <p>San Francisco, CA 94133</p>
            <p>United States</p>
          </li>
          <SocialButtons contacts={this.contacts}></SocialButtons>
        </ul>
      </Col>
    )
  }
}