import "./footer.styl";

import React from 'react';
import { Row } from 'react-bootstrap';

import Copyright from './Copyright.js';
import SubscribeForm from './SubscribeForm.js';
import CompanyInfo from './CompanyInfo.js';
import LinksColumn from './LinksColumn.js';

export default class Footer extends React.Component {

  constructor() {
    super();

    this.firstColLinks = ['About', 'Jobs', 'Press', 'Blog', 'Help', 'Policies', 'Terms & Privacy'];
    this.secondColLinks = ['Become a Member', 'Properties List', 'Sign In', 'Widgets', 'Components', 'Tables', 'Lists'];
  }

  render() {
    return (
      <div className="home-footer">
        <div className="home-wrapper">
          <Row>
            <LinksColumn links={this.firstColLinks}></LinksColumn>
            <LinksColumn links={this.secondColLinks}></LinksColumn>
            <CompanyInfo></CompanyInfo>
            <SubscribeForm></SubscribeForm>
          </Row>
          <Copyright></Copyright>
        </div>
      </div>
    )
  }
}