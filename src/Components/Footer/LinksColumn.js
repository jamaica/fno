import React from 'react';
import { Col } from 'react-bootstrap';

export default class LinksColumn extends React.Component {

  renderLink(name, index) {
    return (
      <li key={index}><a href="javascript:void(0);">{name}</a></li>
    )
  }

  render() {
    return (
      <Col xs={6} sm={6} md={3} lg={3}>
        <div className="osLight footer-header">Company</div>
        <ul className="footer-nav pb20">
          {this.props.links.map((name, index) => {
            return this.renderLink(name, index);
          })}
        </ul>
      </Col>
    )
  }
}