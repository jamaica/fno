export function generate(base64) {
    return `data:image/png;base64,${base64}`;
}

export function generateSVG(svgString) {
    // '#' is an invalid character in Firefox and IE rendering of SVG data property                                                                                                                                                                     
    const escapedString = svgString.replace(/#/g , '%23');
    return `data:image/svg+xml;utf8,${escapedString}`;
}