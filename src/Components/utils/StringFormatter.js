const toNumberRegex = /(\d)(?=(\d{3})+\.)/g;

export function toMoney(number) {
    return '$' + number.toFixed(2).replace(toNumberRegex, '$1,');
}

export function toSquareMetres(number) {
    return number.toFixed(0).replace(toNumberRegex, '$1,') + ' sq ft';
}