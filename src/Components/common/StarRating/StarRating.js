import "./star-rating.styl";

import React from 'react';
import classNames from 'classnames';

export default class Property extends React.Component {

    constructor() {
        super();

        this.maxRating = 5;
    }

    renderStar(index) {

        let classSet = {
            fa: true,
            'fa-star' : index <= this.props.rating,
            'fa-star-o' : index > this.props.rating
        };

        if (this.props.className) {
            classSet[this.props.className] = true;
        }

        let className = classNames(classSet);

        return  (
            <li key={index}>
                <span className={className + ' star-' + index}></span>
            </li>
        )
    }

    render() {
        return (
            <ul className="rating">
                {[...Array(this.maxRating)].map((item, index) => {
                    return this.renderStar(index + 1);
                })}
            </ul>
        )
    }
}