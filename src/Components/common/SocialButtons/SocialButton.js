import { Component, PropTypes } from 'react';

export default class SocialButton extends Component {

    _getHref(info) {
        switch (info.type) {
            case 'email':
                return `mailto:${info.value}`;
            case 'phone':
                return `tel:${info.value}`;
            default:
                return info.value;
        }
    }

    render() {
        const props = this.props;

        const baseClasses = 'btn btn-sm btn-icon btn-round btn-o' +
                          (props.className ? ' ' + props.className : '');

        const iconClass = 'fa' + (props.iconClass ? ' ' + props.iconClass : '');

        const href = this._getHref(props);
        return(
            <a href={href} className={baseClasses} target="_blank" rel="noopener noreferrer">
                <span className={iconClass}></span>
            </a>
        );


    }
}