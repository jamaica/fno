import "./social-buttons.styl";

import { Component, PropTypes } from 'react';
import SocialButton from './SocialButton.js';

export default class SocialButtons extends Component {

    constructor() {
        super();

        this.contactButtonClassesMapping = {
            email:    {base: 'btn-green', icon: 'fa-envelope-o'},
            facebook: {base: 'btn-facebook', icon: 'fa-facebook'},
            twitter:  {base: 'btn-twitter', icon: 'fa-twitter'},
            gplus:    {base: 'btn-google', icon: 'fa-google-plus'},
            phone:    {base: 'btn-phone', icon: 'fa-phone'},
            lIn: {base: 'btn-linkedin', icon: 'fa-linkedin'},
            instagram: {base: 'btn-instagram', icon: 'fa-instagram'},
            pinterest: {base: 'btn-pinterest', icon: 'fa-pinterest'}
        }
    }

    _retrieveContactsInfo() {
        const contacts = [];
        const contactTypes = Object.keys(this.props.contacts);

        contactTypes.forEach(contactType => {
            let info = this.contactButtonClassesMapping[contactType];
            if (info) {
                info = Object.assign({}, info, {
                    value: this.props.contacts[contactType],
                    type: contactType
                });
                contacts.push(info);
            }
        });

        return contacts;
    }

    renderButtons() {
        const contacts = this._retrieveContactsInfo();

        return contacts.map((contactInfo, index) => {

            return (
                <SocialButton
                    key={index}
                    className={contactInfo.base}
                    iconClass={contactInfo.icon}
                    type={contactInfo.type}
                    value={contactInfo.value}
                >
                </SocialButton>
            );
        });
    }

    render() {
        return (
            <div>
                {this.renderButtons()}
            </div>
        );
    }
}