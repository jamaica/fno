import { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { FormControl } from 'react-bootstrap';
import ButtonDropdown from './../controls/ButtonDropdown/ButtonDropdown.js';
import MapAutocomplete from './../controls/MapAutocomplete/MapAutocomplete.js';
import CustomCheckbox from './../controls/CustomCheckbox/CustomCheckbox.js';
import "./search-panel.styl";

class SearchPanel extends Component {

  state = {
    advancedSearchHidden: true
  }

  showAdvancedSearch() {
    this.setState({ advancedSearchHidden: !this.state.advancedSearchHidden });
  }

  handleSubmit(e) {
    e.preventDefault();

    let data = {};
    let fields = ['city', 'bedrooms', 'bathrooms', 'priceFrom', 'priceTo', 'isRent', 'isSale'];

    fields.forEach(field => data[field] = this.refs[field].getValue());

    // TODO: Build the url and open a new page here
    return data;
  }

  render() {

    var advancedSearchClass = classNames({
      'hidden-xs': this.state.advancedSearchHidden
    });
    return (
      <div className="search-panel">
        <form className="form-inline" role="form" onSubmit={this.handleSubmit.bind(this)}>
          <div className="form-group">
            <MapAutocomplete
              ref="address"
              placeholder="Address"
              className="form-control">
            </MapAutocomplete>
          </div>
        </form>
      </div>
    )
  }
}

export default SearchPanel;

/*          <CustomCheckbox ref="isRent" groupClassName={advancedSearchClass} label="For Rent"/>
 <CustomCheckbox ref="isSale" groupClassName={advancedSearchClass} label="For Sale"/>
 <div className="form-group">
 <button type="submit" href="javascript:void(0);" className="btn btn-green">Search</button>
 <a href="javascript:void(0);"
 className="btn btn-o btn-white pull-right visible-xs"
 onClick={this.showAdvancedSearch.bind(this)}>
 Advanced Search
 <span className="fa fa-angle-up"></span>
 </a>
 </div>*/