import React from 'react';

export default class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTitle: ''
    };
  }

  componentWillMount() {
    this.setActive(this.props.active);
  }

  setActive(id) {
    this.props && this.props.list && Array.isArray(this.props.list) && this.props.list.forEach(function (item, index) {
      if (item.id == id) {
        this.setState({
          activeTitle: item.fullName
        });
      }
    }, this);
  }

  selectItem(id) {
    this.setActive(id);
    this.props.onSelect && this.props.onSelect(id);
  }

  render() {
    return (
      <div className="btn-group">
        <label>{this.props.title}</label>
        <div className="clearfix"></div>
        <a href="#" data-toggle="dropdown" className="btn btn-default dropdown-toggle">
                    <span className="dropdown-label">
                        {this.state.activeTitle || ''}
                    </span>
          &nbsp;&nbsp;&nbsp;
          <span className="caret"></span>
        </a>
        <ul className="dropdown-menu dropdown-select">
          {
            this.props &&
            this.props.list &&
            Array.isArray(this.props.list) &&
            this.props.list.map((item, index)=> {
              return (<li key={index}>
                <input type="radio" name="ptype"/>
                <a onClick={this.selectItem.bind(this, item.id)} href="#">{item.fullName}</a>
              </li>)
            })
          }
        </ul>
      </div>
    )
  }
}