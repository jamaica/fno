import { Component, PropTypes } from 'react';
import { Row } from 'react-bootstrap';
import Agent from './Agent.js';

import "./agents.styl";

export default class AgentsList extends Component {

  renderAgents() {
    return this.props.agents.map((agent, index) => {
      return (
        <Agent
          key={index}
          image={agent.image}
          fullName={agent.fullName}
          rating={agent.rating}
          contacts={agent.contacts}
        >
        </Agent>
      );
    });
  }

  render() {
    return (
      <div className="home-content">
        <h2 className="osLight">Our Agents</h2>
        <Row className="pb40">
          {this.renderAgents()}
        </Row>
      </div>
    );
  }
}