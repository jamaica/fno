import { Component, PropTypes } from 'react';
import { Col } from 'react-bootstrap';
import StarRating from './../common/StarRating/StarRating.js';
import SocialButtons from './../common/SocialButtons/SocialButtons.js';

export default class Agent extends Component {

  renderAvatar(image) {
    const avatar = image ?
      <img src={image} alt="avatar"/> :
      <div className="avatar">
        <i className="icon-user s-icon"></i>
      </div>;

    return (
      <a href="javascript:void(0)" className="agent-avatar">
        {avatar}
        <div className="ring"></div>
      </a>
    )
  }

  render() {
    const agentInfo = this.props;

    return (
      <Col xs={12} sm={6} md={3} lg={3}>
        <div className="agent">
          {this.renderAvatar(agentInfo.image)}
          <div className="agent-name osLight">{agentInfo.fullName}</div>
          <div className="agent-contact">
            <SocialButtons contacts={agentInfo.contacts}></SocialButtons>
          </div>
        </div>
      </Col>
    )
  }
}