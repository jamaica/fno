import { IndexRoute, Route, Redirect } from 'react-router';
import App from './App';
import HomePage from './HomePage';
import EditPage from './EditPage';
import NoMatch from '../Components/common/NoMatch';
import Login from './Login';

export default (
  <Route path="/admin" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="/admin/listing" component={EditPage} />
    <Route path="/admin/listing/:id" component={EditPage} />
    <Route path="/admin/login" component={Login} />
    <Redirect from="/admin/logout" to="/admin/login" />
    <Route path="*" component={NoMatch} />
  </Route>
);
