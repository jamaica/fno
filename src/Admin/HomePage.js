import { Component, PropTypes } from 'react';
import axios from 'axios';
import { Table, Tr, Td } from 'reactable';
import { Link } from 'react-router';
import classnames from 'classnames';

import './homePage.styl';

export default class HomePage extends Component {

  static get NAME() {
    return 'ADMIN_LIST';
  }

  static get contextTypes() {
    return {
      data: React.PropTypes.object,
    };
  }

  static requestData(params, domain = '') {
    return axios.get(`${domain}/api/admin/list`);
  }

  constructor(props, context) {
    super(props, context);
    const state = {
      listings: [],
    };
    this.state = context.data[HomePage.NAME] || state;
  }

  componentDidMount() {
    this.constructor.requestData().then((response) => {
      this.setState(response.data);
    }).catch((err) => {
      throw new Error(err);
    });
  }

  toogleActive(id) {
    const listing = this.state.listings.find((e) => e._id === id);
    axios.put(`/api/listing/${id}`, { active: !listing.active }).then(() => {
      const listings = this.state.listings.map(elem => {
        let l = elem;
        if (l._id === id) {
          l = Object.assign({}, l, { active: !l.active });
        }
        return l;
      });
      this.setState({ listings });
    });
  }

  toogleHot(id) {
    const listing = this.state.listings.find((e) => e._id === id);
    axios.put(`/api/listing/${id}`, { hot: !listing.hot }).then(() => {
      const listings = this.state.listings.map(elem => {
        let l = elem;
        if (l._id === id) {
          l = Object.assign({}, l, { hot: !l.hot });
        }
        return l;
      });
      this.setState({ listings });
    });
  }

  remove(id) {
    axios.delete(`/api/listing/${id}`).then(() => {
      const listings = this.state.listings.filter(elem => elem._id !== id);
      this.setState({ listings });
    });
  }

  renderRow(data) {
    const trClassNames = classnames({
      danger: !data.active,
      warning: data.hot,
    });
    const columns = {
      Hot: <span
        onClick={() => this.toogleHot(data._id)}
        className="glyphicon glyphicon-certificate"
        aria-hidden
      />,
      Title: <Link to={`/admin/listing/${data._id}`}>{data.title}</Link>,
      Type: data.type,
      Sale: data.sale,
      Size: data.size,
      Address: data.location.address,
      Price: data.price,
      Broker: data.broker.fullName,
      Disable: <span
        onClick={() => this.toogleActive(data._id)}
        className={() => {
          classnames({
            glyphicon: true,
            'glyphicon-menu-down': data.active,
            'glyphicon-menu-up': !data.active,
          });
        }}
        aria-hidden
      />,
      Remove: <span
        onClick={() => this.remove(data._id)}
        className="glyphicon glyphicon-remove"
        aria-hidden
      />,
    };
    const rows = Object.keys(columns).map(key => <Td key={key} column={key}>{columns[key]}</Td>);

    return (
      <Tr className={trClassNames} key={data._id}>
        {rows}
      </Tr>
    );
  }

  render() {
    const listings = this.state.listings;

    return (
      <div className="admin-listings-table">
        <Link to={"/admin/listing"} className="btn btn-default">Add listings</Link>
        <Table className="table table-hover">
          {listings.map(data => this.renderRow(data))}
        </Table>
      </div>
    );
  }
}