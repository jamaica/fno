import './login.styl';

const Login = () =>
  <div className="login container">
    <form className="form-signin" role="form" method="post" action="/admin/login">
      <h2 className="form-signin-heading">Please sign in</h2>
      <label htmlFor="username" className="sr-only">Username</label>
      <input
        name="username"
        type="text"
        className="form-control"
        placeholder="Username"
        required
        autoFocus
      />
      <label htmlFor="password" className="sr-only">Password</label>
      <input
        name="password"
        type="password"
        className="form-control"
        placeholder="Password"
        required
      />
      <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
  </div>;

export default Login;
