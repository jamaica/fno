import { Component, PropTypes } from 'react';
import axios from 'axios';

import Map from '../Components/Map/Map.js';
import Form from '../Components/Form/Form';
import { markerNew } from '../Components/images/images';
import update from 'react-addons-update';
import { browserHistory } from 'react-router';
import $ from 'jquery';

export default class EditPage extends Component {

  static get NAME() {
    return 'ADMIN_EDIT';
  }

  static get contextTypes() {
    return {
      data: React.PropTypes.object,
    };
  }

  static requestData(params, domain = '') {
    const { id } = params;
    const url = (id) ? `/api/edit/${id}` : '/api/edit';
    return axios.get(domain + url);
  }

  constructor(props, context) {
    super(props, context);
    this.defaultListing = {
      _id: '',
      active: false,
      broker: '',
      created: '',
      description: '',
      images: [],
      location: {
        lat: 40.741385,
        lng: -74.007103,
        region: '',
        address: '',
      },
      price: 0,
      sale: '',
      size: 0,
      title: '',
      type: '',
    };
    const state = {
      listing: this.defaultListing,
      brokers: [],
      regions: [],
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.state = context.data[EditPage.NAME] || state;
  }

  componentDidMount() {
    const { params } = this.props;
    this.constructor.requestData(params).then(({ data }) => {
      this.setState(data);
    });
    this.setHeight();
  }

  setHeight() {
    const winHeight = $(window).height();
    $(this.form).height(winHeight);
    $('body').removeClass('no-hidden')
      .addClass('noscroll');
  }

  getCoordidates(e) {
    const { lat, lng } = e.latLng;
    const listing = update(this.state.listing, {
      location: { $merge: { lat: lat(), lng: lng() } } });
    this.setState({
      listing,
    });
  }

  onSubmit({ formData }) {
    const { id } = this.props.params;
    const url = (id) ? `/api/listing/${id}` : '/api/listing';
    axios.put(url, formData).then(response => browserHistory.push('/'))
      .catch((error) => {
        if (error.response) {
          console.log(error.response.data);
        } else {
          console.log('Error', error.message);
        }
      });
  }

  onChange({ formData }) {
    this.setState({ listing: formData });
  }

  render() {
    const { listing, brokers, regions } = this.state;
    const markerOptions = {
      icon: markerNew,
      draggable: true,
      onMouseup: this.getCoordidates.bind(this),
    };
    const marker = Object.assign({}, listing, { options: markerOptions });
    const schema = {
      title: 'List a New Property',
      type: 'object',
      required: ['title', 'price', 'description', 'size'],
      properties: {
        _id: { type: 'string' },
        title: { type: 'string', title: 'Title', default: '' },
        price: { type: 'integer', title: 'Price' },
        description: { type: 'string', title: 'Description' },
        location: {
          type: 'object',
          title: 'Address',
          properties: {
            address: { type: 'string' },
            lat: { type: 'number', default: 40.741385 },
            lng: { type: 'number', default: -74.007103 },
            region: {
              type: 'string',
              enum: ['', ...regions.map(r => r._id)],
              enumNames: ['Select One', ...regions.map(r => r.label)],
            },
          },
          required: ['address', 'lat', 'lng', 'region'],
        },
        type: {
          type: 'string',
          enum: ['', 'direct', 'sublet'],
          enumNames: ['Select One', 'Direct', 'Sublet'],
        },
        broker: {
          type: 'string',
          enum: ['', ... brokers.map(b => b._id)],
          enumNames: ['Select One', ...brokers.map(b => b.fullName)],
        },
        size: {
          type: 'number',
        },
        sale: {
          type: 'string',
          enum: ['', 'sale', 'rent'],
          enumNames: ['Select One', 'Sale', 'Rent'],
        },
        active: {
          type: 'boolean',
        },
        hot: {
          type: 'boolean',
        },
        images: {
          type: 'array',
          items: {
            type: 'string',
            format: 'data-url',
          },
        },

      },
    };
    const uiSchema = {
      title: {
        classNames: 'col-xs-12 col-sm-12 col-md-8 col-lg-8',
      },
      price: {
        classNames: 'col-xs-12 col-sm-12 col-md-4 col-lg-4',
      },
      description: {
        'ui:widget': 'textarea',
        classNames: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
      },
      _id: {
        'ui:widget': 'hidden',
      },
      location: {
        classNames: 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
        lat: {
          'ui:widget': 'hidden',
        },
        lng: {
          'ui:widget': 'hidden',
        },
        /*'ui:field': 'location',*/
      },
      type: {
        classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
      },
      broker: {
        classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
      },
      size: {
        classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
      },
      sale: {
        classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
      },
      active: {
        classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
      },
      hot: {
        classNames: 'col-xs-12 col-sm-12 col-md-3 col-lg-3',
      },
      images: {
        'ui:field': 'images',
      },
    };

    const formOptions = {
      ref: form => { this.form = form },
      listing,
      schema,
      uiSchema,
      brokers,
      regions,
      onChange: this.onChange,
      onSubmit: this.onSubmit,
    };

    return (
      <div className="edit-wrapper">
        <div className="edit-wrapper__map">
          <Map ref={map => { this.map = map }} id="admin-map" markers={[marker]} />
        </div>
        <div className="edit-wrapper__form">
          <Form {...formOptions} />
        </div>
      </div>
    );
  }
}

EditPage.propTypes = {
  params: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
};
