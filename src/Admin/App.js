import { Component, PropTypes } from 'react';

import './app.styl';

export default class Admin extends Component {
  render() {
    return (
      <div className="admin-wrapper">
        {this.props.children}
      </div>
    );
  }
}