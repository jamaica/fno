const ExtractTextPlugin = require('extract-text-webpack-plugin');
const paths = require('../paths');
const config = require('./_config');

config.entry.user = paths.src.user + '/Entry.js';
config.output.filename = '/js/' + (config.prod ? 'app.min.js' : 'app.js');
config.output.path = paths.bundles.user;
config.plugins.push(
  new ExtractTextPlugin(
    '/css/' + (config.prod ? 'main.min.css' : 'main.css'),
    { allChunks: true, devtool: 'source-map' }
  )
);
module.exports = config;
