var ExtractTextPlugin = require('extract-text-webpack-plugin');
var paths = require('../paths');
var config = require('./_config');

config.entry.admin = paths.src.admin + '/Entry.js';
config.output.filename = '/js/' + (config.prod ? 'app.min.js' : 'app.js');
config.output.path = paths.bundles.admin;
config.plugins.push(
  new ExtractTextPlugin(
    '/css/' + (config.prod ? 'main.min.css' : 'main.css'),
    { allChunks: true, devtool: 'source-map' }
  )
);

module.exports = config;