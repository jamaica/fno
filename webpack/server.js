var webpack = require('webpack');
var paths = require('../paths');
var config = require('./_config');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function (x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function (mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });

config.entry.server = paths.src.server + '/Server.js';
config.output.filename = (config.prod ? 'server.min.js' : 'server.js');
config.output.path = paths.bundles.server;
config.externals = nodeModules;
config.plugins.push(
  new webpack.NormalModuleReplacementPlugin(/\.styl$/, 'node-noop'),
  new webpack.NormalModuleReplacementPlugin(/\.css$/, 'node-noop')
);

module.exports = config;
