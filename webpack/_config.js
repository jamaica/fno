var webpack = require('webpack');
var LiveReloadPlugin = require('webpack-livereload-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');
var paths = require('../paths');
var PROD = process.argv.indexOf('--minimize') !== -1;

var plugins = [
  new LiveReloadPlugin(),
  new webpack.optimize.OccurenceOrderPlugin(true),
  new webpack.ProvidePlugin({
    React: 'react',
  })
];
if (PROD) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
      comments: false,
      sourceMap: false,
    }));

  plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }));
}

var babelQuery = {
  cacheDirectory: true,
  presets: [
    require.resolve('babel-preset-es2015'),
    require.resolve('babel-preset-react'),
    require.resolve('babel-preset-stage-0'),
  ],
  plugins: [
    require.resolve('babel-plugin-transform-es3-member-expression-literals'),
    require.resolve('babel-plugin-transform-es3-property-literals'),
    require.resolve('babel-plugin-add-module-exports'),
  ]
};

module.exports = {
  prod: PROD,
  entry: {},
  output: {},
  devtool: (PROD) ? 'source-map' : 'eval',
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /(node_modules)/,
      loaders: ['es3ify', `babel?${JSON.stringify(babelQuery)}`] // es3ify required for IE8
    }, {
      test: /\.json$/, // XXX Remove this crap when mocks gone
      loader: 'json'
    }, {
      test: /\.styl$/,
      loader: ExtractTextPlugin.extract('style-loader', 'css-loader?sourceMap!postcss!stylus-loader'),
    }, {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract('css-loader?sourceMap!postcss!stylus-loader'),
    }, {
      test: /\.jade$/,
      loader: "pug-loader",
    }, {
      test: /\.(png|jpg|gif)$/,
      loader: 'url-loader?limit=32870&mimetype=image/png',
    }, {
      test: /\.svg$/,
      loader: 'svg-inline'
    }],
  },
  postcss: function () {
    return [autoprefixer];
  },
  resolve: {
    root: paths.nodeModules,
    extensions: ['', '.js', '.jsx', '.styl'],
  },
  resolveLoader: {
    root: paths.nodeModules,
  },
  externals: {},
  plugins: plugins,
};