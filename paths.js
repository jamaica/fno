const path = require('path');
const pkg = require('./package.json');
const rootPath = __dirname;
const destPath = path.join(rootPath, 'public');
const bundle = path.join(destPath, 'bundles');

module.exports = {
  root: rootPath,
  current: './',
  pkgName: pkg.name,
  log: path.join(rootPath, 'log'),
  nodeModules: path.join(rootPath, 'node_modules'),
  src: {
    admin: path.join(rootPath, 'src', 'Admin'),
    user: path.join(rootPath, 'src', 'User'),
    server: path.join(rootPath, 'src', 'Server'),
  },
  bundles: {
    admin: path.join(bundle, 'admin'),
    user: path.join(bundle, 'user'),
    server: path.join(rootPath, 'build'),
  },
};
